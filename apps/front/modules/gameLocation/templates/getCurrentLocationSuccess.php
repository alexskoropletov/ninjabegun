<?php
if( isset( $error ) ) {
    echo $error;
} else {
    if( $oLocation ) {
        $aResources = array();
        $aMusic = array();
        foreach( $oLocation->getResources() as $oGameResource ) {
            if( $oGameResource->is_active ) {
                if( $oGameResource->type == 'audio' ) {
                    $aResources[] = "{ name: '" . $oGameResource->code . "', type: '" . $oGameResource->type . "', src: '/uploads/game_resource/', channel: '" . $oGameResource->channel . "' }";
                    $aMusic[$oGameResource->code] = $oGameResource->code . ": '" . $oGameResource->code . "'";
                } else {
                    $aResources[] = "{ name: '" . $oGameResource->code . "', type: '" . $oGameResource->type . "', src: '" . $oGameResource->getSourceFile()->getWebPath() . "' }";
                }
            }
        }
        ?>
        <script language="javascript">
            var bIsCounting = false;
            var g_resources = [ <?php echo implode( ", ", $aResources );?> ];
            var music_list = { <?php echo implode( ", ", $aMusic );?> };
            PlayerMusicVolume = <?php echo $sMusicVolume;?>;
            var iPlayerMaxLives = <?php echo $iPlayerMaxLives?>;
            var iEnergyUpdates = "<?php echo isset( $iEnergyUpdates ) ? $iEnergyUpdates : 0; ?>";
            var oCounter = false;
            var aAreaList = [];
//            var iCurrentArea = 0;
            var mainMenuCharacterIcon = false;
            var meinMenuLevelSelectLeftArrow = false;
            var meinMenuLevelSelectRightArrow = false;
//            console.log( g_resources );
//            console.log( music_list );

            var baseApp	= {
                data : {
                    score : <?php echo $iScore ? $iScore : 0; ?>
                },
            	onload: function() {
                    document.oncontextmenu=new Function("return false");
                <?php if( !$bVideoEnabled ) { ?>
            		if( !me.video.init( 'jsapp', 640, 480, false, 1.0 ) ) {
            			alert("Sorry but your browser does not support html 5 canvas.");
                        return;
            		}
                <?php } ?>
//            		me.audio.init( "mp3, ogg" );
            		me.audio.init( "ogg" );
            		me.loader.onload = this.loaded.bind( this );
            		me.loader.preload( g_resources );
            		me.state.change( me.state.LOADING );
            	},
            	loaded: function () {
                    <?php if( $oLocation->type == 'home' ) { ?>
                        me.state.set(me.state.MENU, new mainMenuScreen() );
                        me.state.change( me.state.MENU );
                    <?php } else { ?>
                        me.state.set( me.state.PLAY, new PlayScreen() );
//                        bReadyState = true;
                        bReadyState = false;
                        me.state.change( me.state.PLAY );
                    <?php } ?>
            	}
            };

            baseApp.HUD = baseApp.HUD || {};
            baseApp.HUD.Container = me.ObjectContainer.extend({
            	init: function() {
            		this.parent();
            		this.isPersistent = true;
            		this.collidable = false;
            		this.z = Infinity;
            		this.name = "HUD";
            		this.addChild(new baseApp.HUD.ScoreItem(150, 100));
            	}
            });
            baseApp.HUD.ScoreItem = me.Renderable.extend( {
            	init: function(x, y) {
            		this.parent(new me.Vector2d(x, y), 10, 10);
            		this.font = new me.BitmapFont("16[16font", {x:16});
//            		this.font = new me.BitmapFont("16[16font");
            		this.font.alignText = "bottom";
            		this.font.set("right", 1.6);
            		this.score = -1;
            		this.floating = true;
            	},
            	update : function () {
            		if (this.score !== me.game.score) {
            			this.score = me.game.score;
            			return true;
            		}
            		return false;
            	},
            	draw : function (context) {
            		this.font.draw (context, baseApp.data.score, this.pos.x, this.pos.y);
            	}
            });

            <?php if( $oLocation->type == 'home' ) { ?>
            var mainMenuScreen = me.ScreenObject.extend( {
                onResetEvent: function() {
                    if( CurrentTrack && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.stop( CurrentTrack );
                    }

                    me.entityPool.add( "character_icon", CharMiniEntity );
                    me.entityPool.add( "confirm_yes", ConfirmYesEntity );
                    me.entityPool.add( "confirm_no", ConfirmNoEntity );
                    me.entityPool.add( "not_enought", NotEnoughtEntity );
                    me.entityPool.add( "sound_control", MuteSoundEntity );
                    me.entityPool.add( "better_paralax", MainMenuBgParallaxEntity );

                    me.levelDirector.loadLevel( "mainmenu" );

                    me.game.viewport.fadeOut( "#ffffff", 500 );

                    me.game.add( new baseApp.HUD.Container() );
//                    me.game.addHUD( 0, -20, 640, 480 );
//                    me.game.HUD.addItem( "score", new ScoreObject( 135, 32 ) );

                    mainMenuCharacterIcon = new CharMiniEntity( 222, 352, {} );
                    me.game.add( mainMenuCharacterIcon, 1002 );


                    <?php foreach( $oAreaList as $iKey => $oArea ) {?>
                        var iAreaId = <?php echo $oArea->id - 1 ?> * 1;
                        <?php if( $oArea->getScoreToUnlock() > 0 && !isset( $aPlayerAreaList[ $oArea->id ] ) ) { ?>
                            var level_select = new MainMenuLevelBuyButtonEntity( <?php if( !$iKey ) {?>224<?php } else { ?>416<?php } ?>, 352, { level_type: "<?php echo $oArea->code ?>", score: <?php echo $oArea->getScoreToUnlock() ?>, player_score: <?php echo $iScore ? $iScore : 0; ?> } );
                            level_select.name = 'level_select_button';
                            aAreaList[ aAreaList.length ] = { level: level_select, location: "<?php echo( !$iKey ? "center" : "right" ) ?>" };
                            me.game.add( level_select, 1000 );
//                            me.game.add( new MainMenuLevelBuyButtonEntity( 224 + iAreaId * 192, 352, { level_type: "<?php echo $oArea->code ?>", score: <?php echo $oArea->getScoreToUnlock() ?>, player_score: <?php echo $iScore ? $iScore : 0; ?> } ), 1000 );
                        <?php } else { ?>
                            var level_select = new MainMenuLevelSelectButtonEntity( <?php if( !$iKey ) {?>224<?php } else { ?>416<?php } ?>, 352, { level_type: "<?php echo $oArea->code ?>", player_energy: <?php echo $iPlayerEnergy; ?> } );
                            level_select.name = 'level_select_button';
                            aAreaList[ aAreaList.length ] = { level: level_select, location: "<?php echo( !$iKey ? "center" : "right" ) ?>" };
                            me.game.add( level_select, 1000 );
                        <?php } ?>
                    <?php } ?>

                    meinMenuLevelSelectLeftArrow = new MainMenuLeftRightArrowEntity( 208, 368, { arrow_orientation: "left" } );
                    meinMenuLevelSelectLeftArrow.visible = false;
                    meinMenuLevelSelectRightArrow = new MainMenuLeftRightArrowEntity( 416, 368, { arrow_orientation: "right" } );

                    me.game.add( meinMenuLevelSelectLeftArrow, 20000 );
                    me.game.add( meinMenuLevelSelectRightArrow, 20000 );

                    <?php for( $i = 0; $i < $iPlayerMaxEnergy; $i++  ) {?>
                        indicator = new IndicatorEntity( 40 + <?php echo $i * 8; ?>, 52, { type: "<?php echo $i < $iPlayerEnergy ? "Energy" : "Empty" ?>" } );
                        indicator.name = "energy_indicator";
                        me.game.add( indicator, 10 );
                    <?php } ?>
                    <?php for( $i = 0; $i < $iPlayerMaxLives; $i++  ) {?>
                        me.game.add( new IndicatorEntity( 40 + <?php echo $i * 8; ?>, 70, { type: "<?php echo $i < $iPlayerLives ? "Lives" : "Empty" ?>" } ), 10 );
                    <?php } ?>
                    <?php for( $i = 0; $i < $iPlayerMaxArmor; $i++  ) {?>
                        me.game.add( new IndicatorEntity( 40 + <?php echo $i * 8; ?>, 88, { type: "<?php echo $i < $iPlayerArmor ? "Armor" : "Empty" ?>" } ), 10 );
                    <?php } ?>
                    me.game.add( new MouseFollowerEntity( me.input.mouse.pos.x, me.input.mouse.pos.y, { running: false } ), 20001 );
                    me.game.sort();

<!--                    me.game.HUD.updateItemValue( "score", --><?php //echo $iScore ? $iScore : 0; ?><!-- );-->


                    if( iEnergyUpdates > 0 ) {
//                        if( me.game.HUD ) {
//                            me.game.HUD.addItem( "energy_update", new EnergyUpdateCounterEntity( 125, 73 ) );
//                            me.game.HUD.setItemValue( "energy_update", getMinutesSeconds( iEnergyUpdates ) );
//                        }
                        clearInterval( oCounter );
                        oCounter = setInterval( timer, 1000 );
                    }

                    CurrentTrack = "main_menu_music";
                    if( PlayerMusicVolume && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.play( CurrentTrack, true, null, PlayerMusicVolume );
                    }

                    $( "#newcode" ).html( "" );
                },
                onDestroyEvent: function() {
                    if( CurrentTrack && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.stop( CurrentTrack );
                    }
                    clearInterval( oCounter );
                    oCounter = false;
                    aAreaList = [];
//                    me.game.disableHUD();
                }
            } );
            <?php } else { ?>
            var PlayScreen = me.ScreenObject.extend( {
                onResetEvent: function() {
                    if( CurrentTrack && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.stop( CurrentTrack );
                    }

                    if( oCounter ) {
                        clearInterval( oCounter );
                        oCounter = false;
                    }

                    me.input.bindKey( me.input.KEY.LEFT, "left" );
                    me.input.bindKey( me.input.KEY.RIGHT, "right" );
                    me.input.bindKey( me.input.KEY.UP, "jump" );
                    me.entityPool.add( "ready_state_object", ReadyStateObjectEntity );
                    me.entityPool.add( "player_runner", PlayerEntity );
                    me.entityPool.add( "ninja", EnemyEntity );
                    me.entityPool.add( "green_goo", EnemyGreenGooEntity );
                    me.entityPool.add( "finish_object", FinishEntity );
                    me.entityPool.add( "coin", CoinEntity, true );
                    me.entityPool.add( "spikes", SpikeEntity, true );
                    me.entityPool.add( "jump_sign", JumpSignObjectEntity, true );
                    me.levelDirector.loadLevel( "<?=$oLocation->code?>" );

                    CurrentTrack = "<?=$sAreaType?>_ready";
                    CurrentLevel = "<?=$sAreaType?>";

                    if( PlayerMusicVolume && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.play( CurrentTrack, true, null, PlayerMusicVolume );
                    }
                    me.game.viewport.fadeOut( "#ffffff", 500 );


//                    me.game.addHUD( 0, 560, 800, 40 );
//                    me.game.HUD.addItem( "score", new game.ScoreObject( 790, 00 ) );

//                    me.game.addHUD(0, 430, 640, 60);
//                    me.game.HUD.addItem( "score", new ScoreObject( 620, 10 ) );
                    var indicator = '';
                    <?php for( $i = 0; $i < $iPlayerMaxLives; $i++  ) {?>
                        indicator = new IndicatorEntity( me.game.viewport.pos.x + <?php echo $i * 8; ?>, me.game.viewport.pos.y, { type: "<?php echo $i < $iPlayerLives ? "Lives" : "Empty" ?>", running: true } );
                        indicator.name = "lives_indicator";
                        me.game.add( indicator, 10000 );
                    <?php } ?>
                    me.game.add( new MouseFollowerEntity( me.input.mouse.pos.x, me.input.mouse.pos.y, { running: true } ), 20000 );

                    playerObject = me.game.getEntityByName( "player_runner" );
                    me.input.registerPointerEvent(
                        "mousedown",
                        me.game.viewport,
                        function( e ) {
                            if( !bReadyState ) {
                                playerObject[0].doJump();
//                                me.audio.play( "hero_jump", false, false, PlayerMusicVolume );
                            }
                        }
                    );

                    me.game.sort();
<!--                    me.game.HUD.updateItemValue( "score", --><?php //echo $iScore ? $iScore : 0; ?><!-- );-->
                    $( "#newcode" ).html( "" );
                },
                onDestroyEvent: function() {
                    clearInterval( oCounter );
                    me.input.releasePointerEvent( "mousedown", me.game.viewport );
                    oCounter = false;
                    if( CurrentTrack && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.stop( CurrentTrack );
                    }
//                    me.game.disableHUD();
                }
            } );
            <?php } ?>



            window.onReady( function() {
                me.state.set( me.state.LOADING, new tgmLoadingScreen( "<?php echo $oPreloadText->name ?>" ) );
//                me.debug.renderHitBox = true;
//                me.debug.renderCollisionMap = true;
//                me.debug.displayFPS = false;
                baseApp.onload();
            } );

            function timer() {
                if( !bIsCounting ) {
                    bIsCounting = true;
                    iEnergyUpdates--;
                    if( iEnergyUpdates == 0 ) {
//                        if( me.game.HUD ) {
//                            me.game.HUD.setItemValue( "energy_update", getMinutesSeconds( iEnergyUpdates ) );
//                        }
                        $.post( "/dev.php/+/gamePlayer/updatePlayerEnergy", { count: 1 }, function( data ) {
                            iEnergyUpdates = data.updates;
                            aEnergy = me.game.getEntityByName( "energy_indicator" );
                            for( k in aEnergy ) {
                                if( aEnergy[k].name == 'energy_indicator' ) {
                                    if( k >= ( 10 - data.energy ) ) {
                                        aEnergy[k].renderable.setCurrentAnimation( "Energy" );
                                    } else {
                                        aEnergy[k].renderable.setCurrentAnimation( "Empty" );
                                    }
                                }
                            }
                            var aLevelSelectButton = me.game.getEntityByName( "level_select_button" );
                            for( k in aLevelSelectButton ) {
                                if( aLevelSelectButton[k].name == 'level_select_button' ) {
                                    aLevelSelectButton[k].player_energy = data.energy;
                                }
                            }
                            if( iEnergyUpdates == 0 ) {
//                                if( me.game.HUD ) {
//                                    me.game.HUD.removeItem( "energy_update" );
//                                }
                                bIsCounting = false;
                                clearInterval( oCounter );
                                return;
                            }
                        }, "json" );
                    } else {
//                        if( me.game.HUD ) {
//                            me.game.HUD.setItemValue( "energy_update", getMinutesSeconds( iEnergyUpdates ) );
//                        }
                    }
                    bIsCounting = false;
                }
            }

            function getMinutesSeconds( iSeconds ) {
                var time = parseInt( iSeconds, 10 );
                var minutes = Math.floor( time / 60 );
                var seconds = time % 60;
                minutes = minutes > 0 ? minutes : 0;
                seconds = seconds > 0 ? seconds : 0;
                seconds = seconds <= 9 ? "0" + seconds : seconds;
                return minutes + ":" + seconds;
            }

        </script>
        <?php
    }
}
?>
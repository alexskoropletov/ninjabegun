<?php
/**
 * Game location actions
 */
class gameLocationActions extends myFrontModuleActions
{
    public function executeGetCurrentLocation( dmWebRequest $oRequest ) {
        if( !$this->getUser()->isAuthenticated() && $oRequest->getPostParameter( "develop" ) == 1 ) {
            $oUser = Doctrine::getTable( "DmUser" )->find( 3 );
            sfContext::getInstance()->getUser()->signin( $oUser );
        }
        if( $this->getUser()->isAuthenticated() ) {
            $this->iEUDefaultTime = dmConfig::get( "energy_update_time", 180 );
            if( $oRequest->getPostParameter( "area" ) ) {
                $this->oLocation = Doctrine::getTable( 'GameLocation' )->createQuery('l')
                                        ->leftJoin('l.GameLocationArea la')
                                        ->leftJoin('la.Resource a')
                                        ->where('a.code = ?', $oRequest->getPostParameter( "area" ) )
                                        ->andWhere('a.is_active = ?', 1 )
                                        ->andWhere('l.is_active = ?', 1 )
                                        ->orderBy('RAND()')
                                        ->limit(1)
                                        ->fetchOne();
                echo $this->oLocation->id;
                $this->sAreaType = $oRequest->getPostParameter( "area" );
            }
            if( $oRequest->getPostParameter( "location" ) ) {
                $this->oLocation = Doctrine::getTable( "GameLocation" )->findOneByCode( $oRequest->getPostParameter( "location" ) );
            }
            $this->bVideoEnabled = $oRequest->getPostParameter( "next" ) ? true : false;
            if( !$this->oLocation ) {
                $this->oLocation = Doctrine::getTable( "GameLocation" )->find( 1 );
            }
            if( $oRequest->getPostParameter( "next" ) && $this->oLocation->type != 'home' && $this->getUser()->getUser()->getPlayer()->getEnergy() > 0 ) {
                $this->getUser()->getUser()->getPlayer()->setEnergy( $this->getUser()->getUser()->getPlayer()->getEnergy() - 1 );
                if( time() - strtotime( $this->getUser()->getUser()->getPlayer()->getEnergyUpdate() ) > $this->iEUDefaultTime ) {
                    $this->getUser()->getUser()->getPlayer()->setEnergyUpdate( date( "Y.m.d H:i:s" ) );
                }
                $this->getUser()->getUser()->getPlayer()->save();
            }
            if( $this->oLocation->type == 'home' ) {
                $this->aPlayerAreaList = array();
                $this->oPlayerAreaList = Doctrine::getTable( "GamePlayerArea" )->findByPlayer( $this->getUser()->getUser()->getPlayer()->id );
                foreach( $this->oPlayerAreaList as $oPlayerArea ) {
                    if( !$oPlayerArea->locked ) {
                        $this->aPlayerAreaList[ $oPlayerArea->getArea()->getId() ] = "unlocked";
                    }
                }
                $this->oAreaList = Doctrine::getTable( "GameArea" )->findByIsActive( true );
                $this->iPlayerEnergy = $this->getUser()->getUser()->getPlayer()->getEnergy();
                $this->iPlayerMaxEnergy = $this->getUser()->getUser()->getPlayer()->getMaxEnergy();
                if( $this->iPlayerEnergy < $this->iPlayerMaxEnergy ) {
                    $this->sLastEnergyUpdate = $this->getUser()->getUser()->getPlayer()->getEnergyUpdate();
                    if( $this->sLastEnergyUpdate ) {
                        $iTimeDiff = time() - strtotime( $this->sLastEnergyUpdate );
                        if( ( $iTimeDiff / $this->iEUDefaultTime ) > 1 ) {
                            for( $i = 0; $i < floor( $iTimeDiff / $this->iEUDefaultTime ); $i++ ) {
                                if( $this->iPlayerEnergy < $this->iPlayerMaxEnergy ) {
                                    $this->iPlayerEnergy++;
                                }
                            }
                            $this->getUser()->getUser()->getPlayer()->setEnergy( $this->iPlayerEnergy < $this->iPlayerMaxEnergy ? $this->iPlayerEnergy : $this->iPlayerMaxEnergy );
                            $this->getUser()->getUser()->getPlayer()->setEnergyUpdate( date( "Y.m.d H:i:s" ) );
                            $this->getUser()->getUser()->getPlayer()->save();
                        }
                        if( $this->iPlayerEnergy < $this->iPlayerMaxEnergy ) {
                            $this->iEnergyUpdates = ( $iTimeDiff / $this->iEUDefaultTime > 1 ) ? $iTimeDiff - floor( $iTimeDiff / $this->iEUDefaultTime ) : $iTimeDiff;
                            $this->iTimeDiff = $iTimeDiff;
                            $this->iTimeDiffD = floor( $iTimeDiff / $this->iEUDefaultTime );
                            $this->LastUpdate = strtotime( $this->sLastEnergyUpdate );
                        }
                    }
                }
            }
            $this->sMusicVolume = $this->getUser()->getUser()->getPlayer()->getMusicVolume();
            $this->iScore = $this->getUser()->getUser()->getPlayer()->getScore();
            $this->iPlayerLives = $this->getUser()->getUser()->getPlayer()->getLives();
            $this->iPlayerArmor = $this->getUser()->getUser()->getPlayer()->getArmor();
            $this->iPlayerEnergy = $this->getUser()->getUser()->getPlayer()->getEnergy();
            $this->iPlayerMaxLives = $this->getUser()->getUser()->getPlayer()->getMaxLives();
            $this->iPlayerMaxArmor = $this->getUser()->getUser()->getPlayer()->getMaxArmor();
            $this->iPlayerMaxEnergy = $this->getUser()->getUser()->getPlayer()->getMaxEnergy();
            $this->oPreloadText = Doctrine::getTable( 'GamePreloadText' )->createQuery( 'e' )->where( "is_active = ?", 1 )->orderBy( 'RAND()' )->limit( 1 )->fetchOne();
        } else {
            $this->error = "error";
        }
    }
}

<?php
/**
 * Game player actions
 */
class gamePlayerActions extends myFrontModuleActions {

    public function executeGetPlayer( dmWebRequest $oRequest ) {
        $aUserData = $oRequest->getParameter( "userdata" );
        $oBaseLocation = Doctrine::getTable( "GameLocation" )->findOneByCode( "base" );
        if( !$this->getUser()->isAuthenticated() ) {
            if( isset( $aUserData['user_id'] ) ) {
                if( $oPlayer = Doctrine::getTable( "GamePlayer" )->findOneByVkId( $aUserData['user_id'] ) ) {
                    $oUser = Doctrine::getTable( "DmUser" )->findOneByPlayer( $oPlayer->id );
                    $this->getUser()->signin( $oUser );
                } else {
                    $oPlayer = new GamePlayer();
                    $oPlayer->setVkId( $aUserData['user_id'] );
                    $oPlayer->setName( $aUserData['user_name'] );
                    $oPlayer->setGender( $aUserData['user_sex'] == 2 ? "male" : "female" );
                    if( isset( $aUserData['user_photo'] ) ) {
                        $oPlayer->setImageVk( $aUserData['user_photo'] );
                    }
                    $oPlayer->save();
                    $oUser = new DmUser();
                    $oUser->setUsername( "id" . $aUserData['user_id'] );
                    $oUser->setIsActive( true );
                    $oUser->setPlayer( $oPlayer );
                    $oUser->setEmail( "id" . $aUserData['user_id'] . "@somemail.ru" );
                    $oUser->save();
//                    $oShip = Doctrine::getTable( "Ship" )->find( 1 );
//                    $oPlayerShip = new PlayerShip();
//                    $oPlayerShip->setPlayer( $oPlayer );
//                    $oPlayerShip->setShip( $oShip );
//                    $oPlayerShip->setIsOn( true );
//                    $oPlayerShip->save();
//                    $oPlayerState = new PlayerState();
//                    $oPlayerState->setPlayer( $oPlayer );
//                    $oPlayerState->setLevel( Doctrine::getTable( "Level" )->find( 1 ) );
//                    $oPlayerState->setAttack( $oShip->getAttack() );
//                    $oPlayerState->setDefense( $oShip->getDefense() );
//                    $oPlayerState->setEnergy( 100 );
//                    $oPlayerState->setHealth( $oShip->getHealth() );
//                    $oPlayerState->setCurrentHealth( $oShip->getHealth() );
//                    $oPlayerState->save();
//                    echo $this->getHelper()->link( 'main/novyiyIgrok' )->param( "pid", $oPlayer->getId() )->param( "uid", $aUserData['user_id'] )->getHref();
                    $this->getUser()->signin( $oUser );
                }
                $oPlayer->setCurrentLocation( $oBaseLocation );
                $oPlayer->save();
            } else {
                echo "error";
            }
        } else {
            $oPlayer = $this->getUser()->getUser()->getPlayer();
            $oUser = $this->getUser()->getUser();
            if( $oPlayer->id && !$oPlayer->getCurrentLocation() ) {
                $oPlayer->setCurrentLocation( $oBaseLocation );
                $oPlayer->save();
            }
        }
        if( $oPlayer && $oUser ) {
            echo "success";
        }
        return sfView::NONE;
    }

    public function executeUpdatePlayerScore( dmWebRequest $oRequest ) {
        if( $this->getUser()->isAuthenticated() ) {
            $this->getUser()->getUser()->getPlayer()->setScore( (int)$this->getUser()->getUser()->getPlayer()->getScore() + $oRequest->getPostParameter( "count" ) );
            $this->getUser()->getUser()->getPlayer()->save();
        }
        return sfView::NONE;
    }

    public function executeGetPlayerScore( dmWebRequest $oRequest ) {
        if( $this->getUser()->isAuthenticated() ) {
            echo $this->getUser()->getUser()->getPlayer()->getScore();
        }
        return sfView::NONE;
    }

    public function executeUpdatePlayerEnergy( dmWebRequest $oRequest ) {
        if( $this->getUser()->isAuthenticated() ) {
            $this->getUser()->getUser()->getPlayer()->setEnergy( (int)$this->getUser()->getUser()->getPlayer()->getEnergy() + $oRequest->getPostParameter( "count" ) );
            $this->getUser()->getUser()->getPlayer()->setEnergyUpdate( date( "Y.m.d H:i:s" ) );
            $this->getUser()->getUser()->getPlayer()->save();
            if( $this->getUser()->getUser()->getPlayer()->getEnergy() < $this->getUser()->getUser()->getPlayer()->getMaxEnergy() ) {
                echo json_encode( array( "updates" => 180, "energy" => $this->getUser()->getUser()->getPlayer()->getEnergy() ) );
            } else {
                echo json_encode( array( "updates" => 0, "energy" => $this->getUser()->getUser()->getPlayer()->getEnergy() ) );
            }
        }
        return sfView::NONE;
    }

    public function executeSetPlayerMusicVolume( dmWebRequest $oRequest ) {
        if( $this->getUser()->isAuthenticated() ) {
            $this->getUser()->getUser()->getPlayer()->setMusicVolume( $oRequest->getPostParameter( "music_volume" ) );
            $this->getUser()->getUser()->getPlayer()->save();
        }
        return sfView::NONE;
    }
}

<?php
/**
 * Game area actions
 */
class gameAreaActions extends myFrontModuleActions
{
    public function executeUnlockArea( dmWebRequest $oRequest ) {
        if( $this->getUser()->isAuthenticated() ) {
            if( $oRequest->getParameter( "area" ) ) {
                if( $oArea = Doctrine::getTable( "GameArea" )->findOneByCode( $oRequest->getParameter( "area" ) ) ) {
                    if( $this->getUser()->getUser()->getPlayer()->getScore() >= $oArea->getScoreToUnlock() ) {
                        if( $oPlayerArea = Doctrine::getTable( "GamePlayerArea" )->createQuery( "a" )->where( "player = ?", $this->getUser()->getUser()->getPlayer()->id )->andWhere( "area = ?", $oArea->id )->fetchOne() ) {
                            if( $oPlayerArea->locked ) {
                                $oPlayerArea->setLocked( false );
                                $oPlayerArea->save();
                            }
                        } else {
                            $oPlayerArea = new GamePlayerArea();
                            $oPlayerArea->setArea( $oArea );
                            $oPlayerArea->setPlayer( $this->getUser()->getUser()->getPlayer() );
                            $oPlayerArea->setLocked( false );
                            $oPlayerArea->save();
                        }
                        $this->getUser()->getUser()->getPlayer()->setScore( $this->getUser()->getUser()->getPlayer()->getScore() - $oArea->getScoreToUnlock() );
                        $this->getUser()->getUser()->getPlayer()->save();
                    }
                }
            }
        }
        return sfView::NONE;
    }

}

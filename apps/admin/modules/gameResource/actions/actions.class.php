<?php

require_once dirname(__FILE__).'/../lib/gameResourceGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/gameResourceGeneratorHelper.class.php';

/**
 * gameResource actions.
 *
 * @package    tgm
 * @subpackage gameResource
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12474 2008-10-31 10:41:27Z fabien $
 */
class gameResourceActions extends autoGameResourceActions
{
}

var CurrentTrack = "";
var CurrentLevel = "";
var PlayerSoundVolume = "";
var PlayerMusicVolume = "";
var bReadyState = true;
var bScreenLocked = false;
var bOverLeftRightArrow = false;

var tgmLoadingScreen = me.ScreenObject.extend( {
    preload_text: "",
    init: function( preload_text ) {
        this.parent( true );
        this.logo1 = new me.Font( 'century gothic', 15, 'black', 'middle' );
        this.background_image = null;
        this.manloading = null;
        this.flying_strips = null;
        this.invalidate = false;
        this.loadPercent = 0;
        this.preload_text = preload_text;
        me.loader.onProgress = this.onProgressUpdate.bind( this );
    },
    onResetEvent: function() {
        if( this.background_image == null ) {
            this.background_image = me.loader.getImage( "loading_screen" );
        }
        if( this.flying_strips == null ) {
            this.flying_strips = me.loader.getImage( "loading_stripes_01" );
            this.stripes = Array();
            for( var i = -2; i < 5; i++ ) {
                this.stripes[ this.stripes.length ] = this.flying_strips.width * i;
            }
        }
        if( this.manloading == null ) {
            this.manloading = me.loader.getImage( "manloading" );
            this.manx = 383;
        }
    },
    onProgressUpdate: function( progress ) {
        this.loadPercent = progress;
        this.invalidate = true;
        var strip_shift = me.timer.tick * ( me.video.getWidth() / 16 );
        this.manx -= me.timer.tick * 2;
        for( k in this.stripes ) {
            this.stripes[k] += strip_shift;
            if( this.stripes[k] >= me.video.getWidth() + this.flying_strips.width ) {
                this.stripes[k] = -1 * this.flying_strips.width;
            }
        }
    },
    update: function() {
        if( this.invalidate === true ) {
            this.invalidate = false;
            return true;
        }
        return false;
    },
    onDestroyEvent : function () {
        this.background_image = null;
        this.manloading = null;
        this.flying_strips = null;
        this.logo1 = null;
    },
    draw : function( context ) {
        me.video.clearSurface( context, "#fff" );
        context.drawImage( this.background_image, 0, 0 );

        var logo1_width = this.logo1.measureText( context, this.preload_text ).width;
        var xpos = ( me.video.getWidth() - logo1_width ) / 2;
        this.logo1.draw( context, this.preload_text, xpos , 400 );

        var progress = Math.floor( this.loadPercent * 240 );

        for( k in this.stripes ) {
            context.drawImage( this.flying_strips, this.stripes[k], 115 );
        }

        context.drawImage( this.manloading, this.manx, 52 );

        context.strokeStyle = "#f5a301";
        context.strokeRect( 200, 300, 240, 24 );
        context.fillStyle = "#0080ff";
        context.fillRect( 202, 302, progress - 4, 20 );
    }
});

var MouseFollowerEntity = me.ObjectEntity.extend({
    running: false,
    init: function(x, y, settings) {
        settings.spriteheight = 32;
        settings.spritewidth = 32;
        settings.image = "mouse_follower";
        this.parent(x, y, settings);
        this.running = settings.running;
        this.gravity = 0;
//        if( !this.running ) {
//        me.game.viewport.follow( this, me.game.viewport.AXIS.BOTH );
//        }
    },
    update: function() {
        this.falling = false;
        this.pos.x = me.game.viewport.pos.x + me.input.mouse.pos.x;
        this.pos.y = me.game.viewport.pos.y + me.input.mouse.pos.y;
        this.updateMovement();
        this.parent();
        return true;
    }
});

var MainMenuBgParallaxEntity = me.ObjectEntity.extend( {
    init: function(x, y, settings) {
        settings.spritewidth = 800;
        settings.spriteheight = 600;
        settings.image = "new_mainmenu_parallax_bg";
        this.parent( x, y, settings );
        this.gravity = 0;
    },
    update: function() {
        this.falling = false;
        this.pos.x = ( me.input.mouse.pos.x - ( this.width / ( me.game.viewport.width / me.input.mouse.pos.x ) ) ) / 2;
        this.pos.y = ( me.input.mouse.pos.y - ( this.width / ( me.game.viewport.width / me.input.mouse.pos.y ) ) ) / 2;
        this.updateMovement();
        this.parent();
        return true;
    }
} );

//var EnergyUpdateCounterEntity = me.HUD_Item.extend({
//    init: function(x, y) {
//
//        this.parent(x, y);
//        this.font = new me.BitmapFont( "16[16font", 16 );
//        this.font.set( "left" );
//
////        this.parent(x, y);
////        this.font = new me.Font( 'verdana bold', "18px", 'white', 'left' );
//    },
//    draw: function(context, x, y) {
//        this.font.draw( context, this.value, this.pos.x + x, this.pos.y + y );
//    }
//});

//var EnergyUpdateCounterShadowEntity = me.HUD_Item.extend({
//    init: function(x, y) {
//        this.parent(x, y);
//        this.font = new me.Font( 'verdana bold', "18px", 'black', 'left' );
//    },
//    draw: function(context, x, y) {
//        this.font.draw( context, this.value, this.pos.x + x, this.pos.y + y );
//    }
//});



var CoinEntity = me.ObjectEntity.extend({
    isCoin: true,
    init: function(x, y, settings) {
        settings.image = "coins";
        settings.spritewidth = 32;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.collidable = true;
//        this.renderable.addAnimation( "Idle", [0, 1, 2, 3, 4, 5, 6, 7, 6, 5, 4, 3, 2, 1], 1 );
        this.renderable.addAnimation( "Idle", [0, 1, 2, 3], 100 );
        this.renderable.setCurrentAnimation( "Idle" );
        this.updateColRect( 6, 20, 6, 20 );
    },
    onCollision: function() {
        this.collidable = false;
        me.game.remove(this);
    }
});

var FinishEntity = me.ObjectEntity.extend({
    isFinish: true,
    init: function(x, y, settings) {
        settings.image = "Finish";
        settings.spritewidth = 96;
        settings.spriteheight = 96;
        this.parent(x, y, settings);
        this.updateColRect( 25, 46, 0, 96 );
        this.renderable.addAnimation( "Idle", [0, 1, 2, 3, 2, 1 ], 100 );
//        this.renderable.addAnimation( "Finish", [4, 5, 6, 7, 8, 9, 10, 11 ], 3 );
        this.renderable.setCurrentAnimation( "Idle" );
//        this.renderable.setCurrentAnimation( "Finish" );
        this.setVelocity( 0, 0 );
        this.gravity = 0;
        this.collidable = true;
        this.type = me.game.ACTION_OBJECT;
    },
    onCollision: function() {
        this.collidable = false;
    }
});


var ResetObject = Object.extend({
    init : function () {
        this.visible = false;
        this.z = -Infinity;
    },

    update : function () {
//        jsapp.hovering = false;

        return false;
    }
});

var ReadyStateObjectEntity = me.ObjectEntity.extend( {
    init: function(x, y, settings) {
        settings.image = "readyscreen";
        settings.spritewidth = 320;
        settings.spriteheight = 240;
        this.parent( 160, me.game.viewport.pos.y + 120, settings);
        this.renderable.addAnimation( "Ready", [0, 1, 2, 3], 100 );
        this.renderable.addAnimation( "Run", [4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7, 4, 5, 6, 7 ], 100 );
        this.gravity = 0;
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
        this.renderable.setCurrentAnimation( "Ready" );
    },
    clickOn: function( e ) {
        var ppj = this;
        if( typeof music_list[CurrentTrack] != 'undefined' ) {
//            me.audio.stop( CurrentTrack );
        }
        if( PlayerMusicVolume && music_list[CurrentLevel + "_steady"] ) {
//            me.audio.play( CurrentLevel + "_steady", false, function() {
//                me.audio.play( CurrentLevel + "_run", true, null, PlayerMusicVolume );
//            }, PlayerMusicVolume );
        }
        this.PlayAnimation( "Run", function() {
            bReadyState = false;
            CurrentTrack = CurrentLevel + "_run";
            me.game.remove( ppj );
        } );
    },
    update: function() {
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    },
    PlayAnimation: function( strAnimName, callback ) {
        if( !this.renderable.isCurrentAnimation( strAnimName ) ) {
            this.renderable.setCurrentAnimation( strAnimName, callback );
            return true;
        }
        return false;
    }
} );

var JumpSignObjectEntity = me.ObjectEntity.extend( {
    init: function(x, y, settings) {
        settings.image = "jump_sign";
        settings.spritewidth = 128;
        settings.spriteheight = 32;
        this.parent( x, y, settings);
        this.renderable.addAnimation( "JumpSign", [0, 1, 2, 1 ], 50 );
        this.gravity = 0;
        this.renderable.setCurrentAnimation( "JumpSign" );
    },
    update: function() {
        this.parent();
        return true;
    },
    PlayAnimation: function( strAnimName, callback ) {
        if( !this.renderable.isCurrentAnimation( strAnimName ) ) {
            this.renderable.setCurrentAnimation( strAnimName, callback );
            return true;
        }
        return false;
    }
} );


var MainMenuLevelSelectButtonEntity = me.ObjectEntity.extend( {
    b_clicked: false,
    level_type: "",
    load_level: "",
    player_energy: 0,
    reverse: false,
    show: false,
    init: function(x, y, settings) {
        settings.image = eval( "'" + settings.level_type + "'" );
        settings.spritewidth = 192;
        settings.spriteheight = 64;
        this.parent(x, y, settings);
        this.renderable.addAnimation( "out", [0]);
        this.renderable.addAnimation( "over", [1]);
        this.renderable.addAnimation( "click", [2]);
        this.level_type = settings.level_type;
        this.load_level = settings.load_level;
        this.player_energy = settings.player_energy;
        this.gravity = 0;
        this.reverse = false;
        this.show = false;
        this.floating = true;
        this.setVelocity( 0, 0 );
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
        this.renderable.setCurrentAnimation( "out" );
    },
    clickOn: function( e ) {
        if( !bScreenLocked && !bOverLeftRightArrow && this.renderable.alpha > 0.5 ) {
            if( this.player_energy > 0 ) {
                mainMenuCharacterIcon.setVelocity( 2, 0 );
//                mainMenuCharacterIcon.renderable.setCurrentAnimation( "Fly" );
//                me.audio.play( "enemy_hit", false, false, PlayerMusicVolume );
                me.game.reset();
//                me.game.disableHUD();
                $.post( "/dev.php/+/gameLocation/getCurrentLocation", { area: this.level_type, next: 1 }, function( data ) {
                    clearInterval( oCounter );
                    oCounter = false;
//                    me.audio.stop( CurrentTrack );
                    $( "#newcode" ).html( data );
                } );
            } else {
                var oNotEnoughtButton = me.game.getEntityByName( "not_enought" )[0];
                oNotEnoughtButton.type = "energy";
                oNotEnoughtButton.visible = true;
                bScreenLocked = true;
            }
        }
    },
    update: function() {
        if( !bScreenLocked && !bOverLeftRightArrow ) {
            if( this.b_clicked ) {
                this.renderable.setCurrentAnimation( "click" );
            } else {
                if( this.collisionBox.containsPointV( me.input.mouse.pos ) ) {
                    this.renderable.setCurrentAnimation( "over" );
                } else {
                    this.renderable.setCurrentAnimation( "out" );
                }
            }
        } else {
            this.renderable.setCurrentAnimation( "out" );
        }
        if( this.reverse ) {
            this.vel.x -= this.accel.x * me.timer.tick;
        } else {
            this.vel.x += this.accel.x * me.timer.tick;
        }
        this.updateMovement();
        var iRelativePosition = this.pos.x + ( this.renderable.width / 2 );
        if( ( this.show && iRelativePosition <= ( me.game.viewport.width / 2 ) && this.reverse ) || ( this.show && iRelativePosition >= ( me.game.viewport.width / 2 ) && !this.reverse ) ) {
            this.setVelocity( 0, 0 );
            mainMenuCharacterIcon.renderable.setCurrentAnimation( "Run" );
        }
        if( ( this.pos.x + ( this.renderable.width / 2 ) ) < ( me.game.viewport.width / 2 ) ) {
            var iAlpha = ( this.renderable.width - ( ( me.game.viewport.width / 2 ) - iRelativePosition ) ) / this.renderable.width > 0 ? ( this.renderable.width - ( ( me.game.viewport.width / 2 ) - iRelativePosition ) ) / this.renderable.width : 0;
        } else {
            var iAlpha = ( this.renderable.width - ( iRelativePosition - ( me.game.viewport.width / 2 ) ) ) / this.renderable.width > 0 ? ( this.renderable.width - ( iRelativePosition - ( me.game.viewport.width / 2 ) ) ) / this.renderable.width : 0;
        }
        this.renderable.alpha = iAlpha;
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var MainMenuLevelBuyButtonEntity = me.ObjectEntity.extend( {
    level_type: "",
    player_score: "",
    reverse: false,
    show: false,
    init: function(x, y, settings) {
        settings.image = eval( "'" + settings.level_type + "_buy'" );
        settings.spritewidth = 196;
        settings.spriteheight = 76;
        settings.floating = true;
        this.parent(x, y, settings);
        settings.floating = true;
        this.renderable.addAnimation( "out", [0]);
        this.renderable.addAnimation( "over", [1]);
        this.level_type = settings.level_type;
        this.score = settings.score;
        this.player_score = settings.player_score;
        this.reverse = false;
        this.show = false;
        this.setVelocity( 0, 0 );
        this.gravity = 0;
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
        this.renderable.setCurrentAnimation( "out" );
    },
    clickOn: function( e ) {
        if( !bScreenLocked ) {
            if( this.score <= this.player_score ) {
                var oConfirmYesButton = me.game.getEntityByName( "confirm_yes" )[0];
                var oConfirmNoButton = me.game.getEntityByName( "confirm_no" )[0];
                oConfirmYesButton.visible = true;
                oConfirmYesButton.level_type = this.level_type;
                oConfirmNoButton.visible = true;
                oConfirmNoButton.level_type = this.level_type;
            } else {
                var oNotEnoughtButton = me.game.getEntityByName( "not_enought" )[0];
                oNotEnoughtButton.type = "score";
                oNotEnoughtButton.visible = true;
                bScreenLocked = true;
            }
        }
    },
    update: function() {
        if( !bScreenLocked ) {
            if( this.collisionBox.containsPointV( me.input.mouse.pos ) ) {
                this.renderable.setCurrentAnimation( "over" );
            } else {
                this.renderable.setCurrentAnimation( "out" );
            }
        } else {
            this.renderable.setCurrentAnimation( "out" );
        }
        if( this.reverse ) {
            this.vel.x -= this.accel.x * me.timer.tick;
        } else {
            this.vel.x += this.accel.x * me.timer.tick;
        }
        this.updateMovement();
        var iRelativePosition = this.pos.x + ( this.renderable.width / 2 );
        if( ( this.show && iRelativePosition <= ( me.game.viewport.width / 2 ) && this.reverse ) || ( this.show && iRelativePosition >= ( me.game.viewport.width / 2 ) && !this.reverse ) ) {
            this.setVelocity( 0, 0 );
            mainMenuCharacterIcon.renderable.setCurrentAnimation( "Run" );
        }
        if( ( this.pos.x + ( this.renderable.width / 2 ) ) < ( me.game.viewport.width / 2 ) ) {
            var iAlpha = ( this.renderable.width - ( ( me.game.viewport.width / 2 ) - iRelativePosition ) ) / this.renderable.width > 0 ? ( this.renderable.width - ( ( me.game.viewport.width / 2 ) - iRelativePosition ) ) / this.renderable.width : 0;
        } else {
            var iAlpha = ( this.renderable.width - ( iRelativePosition - ( me.game.viewport.width / 2 ) ) ) / this.renderable.width > 0 ? ( this.renderable.width - ( iRelativePosition - ( me.game.viewport.width / 2 ) ) ) / this.renderable.width : 0;
        }
        this.renderable.alpha = iAlpha;
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var ConfirmYesEntity = me.ObjectEntity.extend( {
    level_type: "",
    init: function(x, y, settings) {
        settings.image = "confirm_yes";
        settings.spritewidth = 64;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.gravity = 0;
        this.visible = false;
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
    },
    clickOn: function( e ) {
        $.post( "/dev.php/+/gameArea/unlockArea", { area: this.level_type }, function() {
            $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "mainmenu", next: 1 }, function( data ) {
                me.game.reset();
                $( "#newcode" ).html( data );
            } );
        } );
        var oConfirmNoButton = me.game.getEntityByName( "confirm_no" )[0];
        oConfirmNoButton.visible = false;
        this.visible = false;
    },
    update: function() {
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var MainMenuLeftRightArrowEntity = me.ObjectEntity.extend( {
    arrow_orientation: "left",
    init: function(x, y, settings) {
        settings.image = "left_right_arrow";
        settings.spritewidth = 16;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.gravity = 0;
        this.arrow_orientation = settings.arrow_orientation;
        this.renderable.addAnimation( "idle_right", [0]);
        this.renderable.addAnimation( "active_right", [1]);
        this.renderable.addAnimation( "active_left", [2]);
        this.renderable.addAnimation( "idle_left", [3]);
        this.renderable.setCurrentAnimation( "idle_" + this.arrow_orientation );
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
    },
    clickOn: function( e ) {
        this.renderable.setCurrentAnimation( "active_" + this.arrow_orientation );
        for( k in aAreaList ) {
            if( aAreaList[k] && aAreaList[k].level && aAreaList[k].level.name == 'level_select_button' && aAreaList[k].location == "center" ) {
                if( this.arrow_orientation == 'right' ) {
                    if( aAreaList[parseInt( k ) + 1] ) {
                        meinMenuLevelSelectLeftArrow.visible = true;
                        mainMenuCharacterIcon.reverse = false;
//                        mainMenuCharacterIcon.renderable.setCurrentAnimation( "Fly" );
//                        me.audio.play( "hero_jump", false, false, PlayerMusicVolume );
                        aAreaList[k].level.show = false;
                        aAreaList[k].level.setVelocity( 16, 2 );
                        aAreaList[k].level.reverse = true;
                        aAreaList[k].location = "left";
                        aAreaList[parseInt( k ) + 1].level.show = true;
                        aAreaList[parseInt( k ) + 1].level.setVelocity( 16, 2 );
                        aAreaList[parseInt( k ) + 1].level.reverse = true;
                        aAreaList[parseInt( k ) + 1].location = "center";
                        if( aAreaList[parseInt( k ) + 2] ) {
                            meinMenuLevelSelectRightArrow.visible = true;
                        } else {
                            meinMenuLevelSelectRightArrow.visible = false;
                        }
                    }
                } else {
                    if( aAreaList[parseInt( k ) - 1] ) {
                        meinMenuLevelSelectRightArrow.visible = true;
                        mainMenuCharacterIcon.reverse = true;
//                        mainMenuCharacterIcon.renderable.setCurrentAnimation( "Fly" );
//                        me.audio.play( "hero_jump", false, false, PlayerMusicVolume );
                        aAreaList[k].level.show = false;
                        aAreaList[k].level.setVelocity( 16, 2 );
                        aAreaList[k].level.reverse = false;
                        aAreaList[k].location = "right";
                        aAreaList[parseInt( k ) - 1].level.show = true;
                        aAreaList[parseInt( k ) - 1].level.setVelocity( 16, 2 );
                        aAreaList[parseInt( k ) - 1].level.reverse = false;
                        aAreaList[parseInt( k ) - 1].location = "center";
                        if( aAreaList[parseInt( k ) - 2] ) {
                            meinMenuLevelSelectLeftArrow.visible = true;
                        } else {
                            meinMenuLevelSelectLeftArrow.visible = false;
                        }
                    }
                }
                break;
            }
        }
        this.renderable.setCurrentAnimation( "idle_" + this.arrow_orientation );
    },
    update: function() {
        if( !bScreenLocked ) {
            if( this.collisionBox.containsPointV( me.input.mouse.pos ) ) {
                this.renderable.setCurrentAnimation( "active_" + this.arrow_orientation );
                bOverLeftRightArrow = true;
            } else {
                this.renderable.setCurrentAnimation( "idle_" + this.arrow_orientation );
                bOverLeftRightArrow = false;
            }
        } else {
            this.renderable.setCurrentAnimation( "idle_" + this.arrow_orientation );
            bOverLeftRightArrow = false;
        }
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var ConfirmNoEntity = me.ObjectEntity.extend( {
    level_type: "",
    init: function(x, y, settings) {
        settings.image = "confirm_no";
        settings.spritewidth = 64;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.gravity = 0;
        this.visible = false;
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
    },
    clickOn: function( e ) {
        var oConfirmYesButton = me.game.getEntityByName( "confirm_yes" )[0];
        oConfirmYesButton.visible = false;
        this.visible = false;
    },
    update: function() {
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var NotEnoughtEntity = me.ObjectEntity.extend( {
    init: function(x, y, settings) {
        settings.image = "not_enought";
        settings.spritewidth = 160;
        settings.spriteheight = 120;
        this.parent(x, y, settings);
        this.renderable.addAnimation( "score", [0]);
        this.renderable.addAnimation( "energy", [1]);
        this.gravity = 0;
        this.type = this.type ? this.type : "score";
        this.visible = false;
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
        this.renderable.setCurrentAnimation( this.type );
    },
    clickOn: function( e ) {
        bScreenLocked = false;
        this.visible = false;
    },
    update: function() {
        if( !this.renderable.isCurrentAnimation( this.type ) ) {
            this.renderable.setCurrentAnimation( this.type );
        }
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var MuteSoundEntity = me.ObjectEntity.extend( {
    current_animation: "MusicOn",
    init: function( x, y, settings ) {
        settings.image = "sound_control";
        settings.spritewidth = 32;
        settings.spriteheight = 32;
        this.parent( x, y, settings );
        this.gravity = 0;
        this.current_animation = PlayerMusicVolume > 0 ? "MusicOn" : "MusicOff";
        this.renderable.addAnimation( "MusicOn", [0]);
        this.renderable.addAnimation( "MusicOnHover", [1]);
        this.renderable.addAnimation( "MusicOff", [2]);
        this.renderable.addAnimation( "MusicOffHover", [3]);
        this.renderable.setCurrentAnimation( this.current_animation );
        me.input.registerPointerEvent( 'click', this.collisionBox, this.clickOn.bind(this) );
    },
    clickOn: function( e ) {
        if( !bScreenLocked ) {
            if( PlayerMusicVolume > 0 ) {
                $.post( "/dev.php/+/gamePlayer/setPlayerMusicVolume", { music_volume: 0 } );
                PlayerMusicVolume = 0;
                this.current_animation = "MusicOff";
//                me.audio.stop( CurrentTrack );
            } else {
                $.post( "/dev.php/+/gamePlayer/setPlayerMusicVolume", { music_volume: 0.2 } );
                PlayerMusicVolume = 0.2;
                this.current_animation = "MusicOn";
//                me.audio.play( CurrentTrack, true, null, PlayerMusicVolume );
            }
        }
    },
    update: function() {
        if( this.collisionBox.containsPointV( me.input.mouse.pos ) ) {
            this.renderable.setCurrentAnimation( this.current_animation + "Hover" );
        } else {
            this.renderable.setCurrentAnimation( this.current_animation );
        }
        this.parent();
        return true;
    },
    onDestroyEvent: function() {
        me.input.releasePointerEvent( "click", this.collisionBox );
    }
} );

var IndicatorEntity = me.ObjectEntity.extend( {
    deltaX: 0,
    deltaY: 0,
    running: false,
    type: "Energy",
    init: function( x, y, settings ) {
        settings.image = "indicators";
        settings.floating = true;
        settings.isPersistent = true;
        settings.spritewidth = 8;
        settings.spriteheight = 16;
        this.parent( x, y, settings );
        this.running = settings.running;
        this.type = settings.type ? settings.type : "Empty";
        this.deltaX = this.pos.x - me.game.viewport.pos.x;
        this.deltaY = me.game.viewport.pos.y - this.pos.y;
        this.gravity = 0;
        this.renderable.addAnimation( "Lives", [0]);
        this.renderable.addAnimation( "LivesHalf", [2]);
        this.renderable.addAnimation( "Energy", [1]);
        this.renderable.addAnimation( "EnergyHalf", [3]);
        this.renderable.addAnimation( "Armor", [4]);
        this.renderable.addAnimation( "ArmorHalf", [5]);
        this.renderable.addAnimation( "Empty", [6]);
        this.renderable.addAnimation( "Armor", [8]);
        this.renderable.addAnimation( "ArmorHalf", [9]);
        this.renderable.setCurrentAnimation( this.type );
    },
    update: function() {
        if( this.running ) {
//            if( !this.renderable.isCurrentAnimation( this.type ) ) {
//                this.renderable.setCurrentAnimation( this.type );
//            }
            this.pos.x = me.game.viewport.pos.x + this.deltaX + 10;
            this.pos.y = me.game.viewport.pos.y + this.deltaY + 10;
            this.updateMovement();
            this.parent();
            return true;
        }
        return false;
    }
} );

//var ScoreObject = me.HUD_Item.extend({
//    init: function(x, y) {
//        this.parent(x, y);
//        this.font = new me.BitmapFont( "font3232", 32 );
//        this.font.set( "right" );
//    },
//    draw: function(context, x, y) {
//        this.font.draw( context, this.value, this.pos.x + x, this.pos.y + y );
//    }
//});

//var LiveObject = me.HUD_Item.extend( {
//    init: function( x, y ) {
//        this.parent( x, y );
//        this.font = new me.BitmapFont( "32x32_font", 32 );
//    },
//    draw: function( context, x, y ) {
//        this.font.draw( context, this.value, this.pos.x + x, this.pos.y + y );
//    }
//
//});

var goToActionLevel = function( x, y, settings ) {
    var nowLoading = new me.LevelEntity( x, y, settings );
    nowLoading.goTo();
}

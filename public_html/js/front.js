( function( $ ) {
    if( window.location.search.replace( "?", "" ) == 'develop' ) {
        $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "mainmenu", develop: 1 }, function( data ) {
            $( "#newcode" ).html( data );
        } );
    } else {
        VK.init( function( ) {
            VK.api( 'isAppUser', {}, function( data ) {
                if( data.response == 1 ) {
                    VK.api( 'getUserInfoEx', { format: 'JSON', test_mode: 1 }, function( data ) {
                        $.post( "/dev.php/+/gamePlayer/getPlayer", { userdata: data.response }, function( data ) {
                            if( data.length ) {
                                if( data != 'error' ) {
                                    $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "mainmenu" }, function( data ) {
    //                                $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "startmap" }, function( data ) {
                                        $( "#newcode" ).html( data );
                                    } );

                                }
                            }
                        } );
                    } );
                }
            } );
        } );
    }
} )( jQuery );
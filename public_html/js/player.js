var PlayerEntity = me.ObjectEntity.extend({
//    drawDeath: false,
//    isAlive: true,
//    hitPoint: 5,
//    bWasFalling: false,
//    isFinished: false,
//    attacking: false,
//    afterFinish: "",
    init: function( x, y, settings ) {
        settings.spriteheight = 64;
        settings.spritewidth = 64;
        settings.image = "character_animation";

        this.parent( x, y, settings );

        this.alwaysUpdate = true;

        this.origVelocity = new me.Vector2d( 5.0, 11.0 );
        this.setVelocity( 5, 15 );
        this.setFriction( 0.5, 0 );
//        this.origGravity = 0.4;
//        this.gravity = this.origGravity;
//        this.setFriction( 0.2, 0.1 );
        this.wallStuckGravity = 0.0;
        this.collidable = true;
        this.updateColRect( 15, 28, 19, 45 );

        this.anchorPoint.set( 0.5, 1.0 );

//        this.haveDoubleJump = unlocked('doubleJump');
//        this.haveRocketJump = unlocked('rocketJump');
//        this.haveWallStick = unlocked('wallStick');
//        this.spikeHat = unlocked('spikeHat');
//        this.haveGills = unlocked('gills');

//        if( unlocked('shield') ) {
//           this.addShield( this.pos.x, this.pos.y, "shield", 144,
//               [ 0, 1, 2, 3, 4, 5 ], 5, 5 );
//        }

        this.wasFalling = false;
        this.doubleJumped = false;
        this.rocketJumped = false;
        this.wallStuck = false;
        this.wallStuckDir = 0.0;
        this.wallStuckCounter = 0;
        this.maxBreath = 1200;
        this.breath = this.maxBreath;
        this.drowning = false;
        this.swimming = false;
        this.fallCounter = 0;
        this.impactCounter = 0;
        this.bubbleCounter = 0;

        this.hitPoint = iPlayerMaxLives;
        this.hp = iPlayerMaxLives;
        if ( this.shield ) {
           this.hp++;
        }

        me.game.viewport.follow( this.pos, me.game.viewport.AXIS.BOTH );

        this.renderable.addAnimation( "Idle", [0], 300 );
        this.renderable.addAnimation( "Run", [0, 1, 2, 1, 0, 3, 4, 3], 100 );
        this.renderable.addAnimation( "Jump", [8], 100 );
//        this.renderable.addAnimation( "DoubleJump", [ 12, 13, 14, 15 ], 100 );
//        this.renderable.addAnimation( "LandingIdle", [ 7 ], 100 );
//        this.renderable.addAnimation( "LandingRun", [ 7, 6, 5, 9 ], 100 );
        this.renderable.addAnimation( "Death", [ 0 ], 100 );
        this.renderable.addAnimation( "Win", [ 8, 0 ], 200 );

//        me.input.bindKey( me.input.KEY.UP, "up" );
//        me.input.bindKey( me.input.KEY.DOWN, "down" );
//        me.input.bindKey( me.input.KEY.LEFT, "left" );
//        me.input.bindKey( me.input.KEY.RIGHT, "right" );
//        me.input.bindKey( me.input.KEY.X, "jump", true );
//        me.input.bindKey( me.input.KEY.V, "rocket" );
//        me.input.bindKey( me.input.KEY.C, "buttstomp" );
//        me.input.bindKey( me.input.KEY.A, "abilities" );

        me.game.player = this;
    },
    getCenter: function() {
        return new me.Vector2d( this.pos.x + this.centerOffsetX, this.pos.y + this.centerOffsetY );
    },

    hit: function( type, amount ) {
        amount = amount ? amount : 0.5;
        if( !this.renderable.flickering && !this.attacking ) {
            this.hp -= amount;
            aLives = me.game.getEntityByName( "lives_indicator" );
            for( k in aLives ) {
                if( aLives[k].name == 'lives_indicator' ) {
                    if( k <= Math.ceil( this.hp ) - 1 ) {
                        aLives[k].renderable.setCurrentAnimation( ( k <= Math.floor( this.hp ) - 1 ? "Lives" : "LivesHalf" ) );
                    } else {
                        aLives[k].renderable.setCurrentAnimation( "Empty" );
                    }
                }
            }
            if ( this.hp <= 0 ) {
                this.renderable.setCurrentAnimation( "die" );
                this.renderable.flicker( 90, function () {
                    this.die( type );
                } );
            } else {
                this.renderable.flicker( 90 );
                if ( type == 'drown' ) {
                    this.breath += 120;
                }
            }
        }
    },
    die: function( type ) {
        this.alive = false;
        this.collidable = false;
        this.setVelocity( 0, 0 );
        var Lives = me.game.getEntityByName( "lives_indicator" );
        for( k in Lives ) {
            Lives[k].type = "Empty";
        }
        this.PlayAnimation( "Death", function() {
            me.game.reset();
            $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "mainmenu", next: 1, dead: 1 }, function( data ) {
                $( "#newcode" ).html( data );
            } );
        } );
    },
    update: function() {
        if( !bReadyState && !this.isFinished ) {
            if( me.input.isKeyPressed( 'left' ) ) {
                this.goLeft = true;
                this.flipX( true );
                this.vel.x -= this.accel.x * me.timer.tick;
            } else if( me.input.isKeyPressed( 'right' ) ) {
                this.goLeft = false;
                this.flipX( false );
                this.vel.x += this.accel.x * me.timer.tick;
            } else {
                this.vel.x = 0;
            }
//            this.vel.x += this.accel.x * me.timer.tick;
            if( me.input.isKeyPressed('jump') ) {
                this.doJump();
//                me.audio.play( "hero_jump", false, false, PlayerMusicVolume );
            }
        }

        this.updateMovement();


//        if( this.pos.y <= 5 ) {
//            this.pos.y = 5;
//        }

        if ( !this.inViewport && ( this.pos.y > me.video.getHeight() ) ) {
            this.die( "fall" );
            this.parent();
            return true;
        }

        if( this.jumping || this.falling ) {
            if( this.vel.y == this.accel.y ) {
                this.bWasFalling = true;
            }
            this.PlayAnimation( "Jump" );
        } else if( this.vel.x != 0 ) {
            if( this.bWasFalling ) {
                this.PlayAnimation( "Run" );
//                this.PlayAnimation( "LandingRun", function() { me.game.player.PlayAnimation( "Run" ); me.game.player.bWasFalling = false; } );
            } else {
                this.PlayAnimation( "Run" );
            }
        } else {
            if( !this.isFinished ) {
                if( this.bWasFalling ) {
//                    var ppj = this;
//                    this.PlayAnimation( "LandingIdle", function() {
//                        ppj.PlayAnimation( "Idle" );
//                        ppj.bWasFalling = false;
//                    } );
                    this.PlayAnimation( "Idle" );
                    this.bWasFalling = false;
                } else {
                    this.PlayAnimation( "Idle" );
                }
            } else {
                this.PlayAnimation( "Win" );
            }
        }

        var res = me.game.collide( this );
        if( res ) {
            if( res.obj.type == me.game.ENEMY_OBJECT ) {
                if( res.obj.isSpike ) {
                    this.hit( 0.5 );
                } else  if( res.obj.alive ) {
                    if( ( res.y > 0 ) && !this.jumping ) {
                        this.falling = false;
                        this.vel.y = -this.maxVel.y * me.timer.tick;
                        this.jumping = true;
                        $.post( "/dev.php/+/gamePlayer/updatePlayerScore", { count: 1 } );
                        res.obj.alive = false;
                    } else {
                        this.hit( 0.5 );
                    }
                }
            }
            if( res.obj.isCoin ) {
//                    me.audio.play( "cling", false, false, PlayerMusicVolume );
//                    me.game.HUD.updateItemValue( "score", 1 );
                baseApp.data.score += 1;
                $.post( "/dev.php/+/gamePlayer/updatePlayerScore", { count: 1 } );
                me.game.remove( res.obj );
            }
            if( res.obj.isFinish ) {
                this.isFinished = true;
                bReadyState = true;
                this.vel.x = 0;
                this.vel.y = 0;
//                    me.game.HUD.updateItemValue( "score", 50 );
                baseApp.data.score += 10;
                if( CurrentTrack && typeof music_list[CurrentTrack] != 'undefined' ) {
//                        me.audio.stop( CurrentTrack );
                }
                $.post( "/dev.php/+/gamePlayer/updatePlayerScore", { count: 50 } );
                this.PlayAnimation( "Win", function() {
                    $.post( "/dev.php/+/gameLocation/getCurrentLocation", { location: "mainmenu", next: 1, finish: 1 }, function( data ) {
                        me.game.reset();
                        $( "#newcode" ).html( data );
                    } );
                } );
            }
        }
        this.parent();
        return true;
    },
    PlayAnimation: function( strAnimName, callback ) {
        if( !this.renderable.isCurrentAnimation( strAnimName ) ) {
            this.renderable.setCurrentAnimation( strAnimName, callback );
        }
    }
} );

var CharMiniEntity = me.ObjectEntity.extend( {
    reverse: false,
    init: function(x, y, settings) {
        settings.spriteheight = 64;
        settings.spritewidth = 64;
        settings.image = 'character_animation';
        this.parent( x, y, settings );
//        this.renderable.addAnimation( "Idle", [0], 100 );
        this.renderable.addAnimation( "Run", [0, 1 ], 250 );
//        this.renderable.addAnimation( "Fly", [ 24, 25, 26, 27], 100 );
        this.gravity = 0;
        this.reverse = false;
        this.renderable.setCurrentAnimation( "Run" );
    },
    update: function() {
        if( this.reverse ) {
            this.flipX( true );
            this.vel.x -= this.accel.x * me.timer.tick;
        } else {
            this.flipX( false );
            this.vel.x += this.accel.x * me.timer.tick;
        }
        this.updateMovement();
        this.parent();
        return true;
    }
} );

var PortretEntity = me.ObjectEntity.extend({
    init: function(x, y, settings) {
        settings.spriteheight = 200;
        settings.spritewidth = 200;
        settings.image = 'portret';
        this.parent(x, y, settings);
        this.gravity = 0;
    },
    update: function() {
        this.parent();
        return true;
    }
});
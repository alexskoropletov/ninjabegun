var SpikeEntity = me.ObjectEntity.extend({
    isSpike: true,
    init: function(x, y, settings) {
        settings.image = "spike";
        settings.spritewidth = 32;
        settings.spriteheight = 32;
        this.parent(x, y, settings);
        this.setVelocity( 0, 0 );
        this.renderable.addAnimation( "Idle", [0, 0, 0, 4, 8, 4], 100 );
        this.renderable.setCurrentAnimation( "Idle" );
        this.gravity = 0;
        this.collidable = true;
        this.updateColRect( 0, 32, 3, 29 );
        this.type = me.game.ENEMY_OBJECT;
    }
});


/* Defines the enemies.  */

var Enemy = me.ObjectEntity.extend( {
	init: function( x, y, settings ){
		this.parent( x, y, settings );
		this.changeDirection( settings.direction || 0 );
		this.setVelocity( 1, 0 );
		this.setMaxVelocity( 1, 8 );
		this.gravity = 0.6;
		this.collidable = true;
		this.type = me.game.ENEMY_OBJECT;
		this.addAnimation( "idle", [0] );
		this.addAnimation( "die", [3] );
		this.addAnimation( "run", [1, 0, 2, 0] );
		this.setCurrentAnimation( "run" );
//        this.updateColRect( 0, 96, 20, 67 );
	},
    changeDirection: function( d ) {
		this.direction = d;
		this.flipX( d );
	},
	die: function() {
//        me.audio.play( "enemydeath" );
		this.alive = false;
		this.collidable = false;
		this.setCurrentAnimation( "die" );
		this.setVelocity( 0, 0 );
		this.flicker( 90, function() {
			me.game.remove( this );
		});
	},
	onCollision: function( res, obj ) {
		this.collide( res, obj );
	},
	collide: function( res, obj ) {
		if ( obj == me.game.player ) {
			 if ( res.y > 0 && obj.buttStomped) {
				this.die();
			}
			else {
				obj.hit( "enemy" );
				this.die();
			}
		}
	},
	update: function() {
		if ( !this.visible ) {
			return false;
		}

		this.parent();

		this.doWalk( this.direction );

		var collision = this.updateMovement();
		if( collision.x ) {
			this.changeDirection( ! this.direction );
		}

		return false;
	}
});

var EnemyEntity = me.ObjectEntity.extend({
    isNinja: true,
    init: function(x, y, settings) {
        settings.image = "ninja_enemy";
        settings.spritewidth = 32;
        settings.spriteheight = 32;
        this.parent(x, y, settings);

        this.renderable.addAnimation( "Walk", [3, 2, 3, 1], 100 );
        this.renderable.addAnimation( "Death", [0, 4, 8], 100 );
        this.renderable.setCurrentAnimation( "Walk" );

        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        // size of sprite

        // make him start from the right
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = false;

        // walking & jumping speed
        this.setVelocity(2, 6);

        // make it collidable
        this.collidable = true;
        // make it a enemy object
        this.type = me.game.ENEMY_OBJECT;

    },
    onCollision: function(res, obj) {
        if( this.alive && (res.y > 0) && obj.falling ) {
//            this.renderable.flicker(45);
        }
    },
    update: function() {
        if( !this.alive ) {
//            this.vel.x = 0;
//            this.vel.y = 0;
//            var killMe = this;
//            console.log( killMe );
////            me.audio.play( "stomp", false, false, PlayerMusicVolume );
//            this.renderable.setCurrentAnimation( "Death", function() {
//                console.log( killMe );
//                me.game.remove( killMe, true );
//            } );
            this.parent();
        } else {
            this.PlayAnimation( "Walk" );
            // do nothing if not visible
            if (!this.visible)
                return false;

            if (this.alive) {
                if (this.walkLeft && this.pos.x <= this.startX) {
                    this.walkLeft = false;
                } else if (!this.walkLeft && this.pos.x >= this.endX) {
                    this.walkLeft = true;
                }
                // make it walk
                this.flipX(this.walkLeft);
                this.vel.x += (this.walkLeft) ? -this.accel.x * me.timer.tick : this.accel.x * me.timer.tick;

            } else {
                this.vel.x = 0;
            }

            // check and update movement
            this.updateMovement();
        }
        // update animation if necessary
        if (this.vel.x!=0 || this.vel.y!=0) {
            // update object animation
            this.parent();
            return true;
        }
        return false;
    },
    PlayAnimation: function( strAnimName, callback ) {
        if( !this.renderable.isCurrentAnimation( strAnimName ) ) {
            this.renderable.setCurrentAnimation( strAnimName, callback );
        }
    }
});

var EnemyGreenGooEntity = me.ObjectEntity.extend({
    isNinja: true,
    init: function(x, y, settings) {
        settings.image = "green_goo";
        settings.spritewidth = 32;
        settings.spriteheight = 32;
        settings.alwaysUpdate = true;
        this.parent(x, y, settings);
        this.renderable.addAnimation( "Walk", [0, 4, 0, 1], 100 );
        this.renderable.addAnimation( "Death", [1, 5, 5, 5, 5, 5, 5, 5], 50 );
        this.renderable.setCurrentAnimation( "Walk" );
        this.startX = x;
        this.endX = x + settings.width - settings.spritewidth;
        this.pos.x = x + settings.width - settings.spritewidth;
        this.walkLeft = false;
        this.setVelocity(2, 6);
        this.collidable = true;
        this.type = me.game.ENEMY_OBJECT;
    },
    onCollision: function(res, obj) {
        if( this.alive && (res.y > 0) && obj.falling ) {
            this.renderable.flicker( 45 );
        }
        if( this.alive && ( res.y > 0 ) && obj.falling ) {
            this.setVelocity( 0, 0 );
            this.alive = false;
            this.collidable = false;
            var self = this;
            this.renderable.setCurrentAnimation( "Death", function() {
                me.game.remove( self );
            } );
//            this.renderable.flicker( 45, function() {
//                self.alive = false;
//            } );
            baseApp.data.score += 1;
        }
    },
    update: function() {
        if( this.alive )	{
            if (this.walkLeft && this.pos.x <= this.startX) {
                this.vel.x = this.accel.x * me.timer.tick;
                this.walkLeft = false;
                this.flipX(true);
            } else if (!this.walkLeft && this.pos.x >= this.endX) {
                this.vel.x = -this.accel.x * me.timer.tick;
                this.walkLeft = true;
                this.flipX(false);
            }
        } else {
            this.vel.x = 0;
        }
        this.updateMovement();
        return (this.parent() || this.vel.x != 0 || this.vel.y != 0);
    },
    PlayAnimation: function( strAnimName, callback ) {
        if( !this.renderable.isCurrentAnimation( strAnimName ) ) {
            this.renderable.setCurrentAnimation( strAnimName, callback );
        }
    }
});
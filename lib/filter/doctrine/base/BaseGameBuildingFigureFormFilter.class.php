<?php

/**
 * GameBuildingFigure filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameBuildingFigureFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active'      => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'           => new sfWidgetFormDmFilterInput(),
      'shape'          => new sfWidgetFormDoctrineChoice(array('model' => 'GameBuildingShape', 'add_empty' => true)),
      'crystal_combo'  => new sfWidgetFormDmFilterInput(),
      'white_crystal'  => new sfWidgetFormDmFilterInput(),
      'building_level' => new sfWidgetFormDoctrineChoice(array('model' => 'GameBuildingLevel', 'add_empty' => true)),
      'bonus_hp'       => new sfWidgetFormDmFilterInput(),
      'bonus_energy'   => new sfWidgetFormDmFilterInput(),
      'bonus_armor'    => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'is_active'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'           => new sfValidatorPass(array('required' => false)),
      'shape'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Shape'), 'column' => 'id')),
      'crystal_combo'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'white_crystal'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'building_level' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Building'), 'column' => 'id')),
      'bonus_hp'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bonus_energy'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'bonus_armor'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('game_building_figure_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GameBuildingFigure';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'is_active'      => 'Boolean',
      'name'           => 'Text',
      'shape'          => 'ForeignKey',
      'crystal_combo'  => 'Number',
      'white_crystal'  => 'Number',
      'building_level' => 'ForeignKey',
      'bonus_hp'       => 'Number',
      'bonus_energy'   => 'Number',
      'bonus_armor'    => 'Number',
    );
  }
}

<?php

/**
 * GamePlayerArea filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGamePlayerAreaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'player' => new sfWidgetFormDoctrineChoice(array('model' => 'GamePlayer', 'add_empty' => true)),
      'area'   => new sfWidgetFormDoctrineChoice(array('model' => 'GameArea', 'add_empty' => true)),
      'locked' => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
    ));

    $this->setValidators(array(
      'player' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Player'), 'column' => 'id')),
      'area'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Area'), 'column' => 'id')),
      'locked' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));
    

    $this->widgetSchema->setNameFormat('game_player_area_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GamePlayerArea';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'player' => 'ForeignKey',
      'area'   => 'ForeignKey',
      'locked' => 'Boolean',
    );
  }
}

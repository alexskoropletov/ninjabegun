<?php

/**
 * GameArea filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameAreaFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active'       => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'            => new sfWidgetFormDmFilterInput(),
      'code'            => new sfWidgetFormDmFilterInput(),
      'image'           => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'score_to_unlock' => new sfWidgetFormDmFilterInput(),
      'locations_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation')),
    ));

    $this->setValidators(array(
      'is_active'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'            => new sfValidatorPass(array('required' => false)),
      'code'            => new sfValidatorPass(array('required' => false)),
      'image'           => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'score_to_unlock' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'locations_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'required' => false)),
    ));
    

    $this->widgetSchema->setNameFormat('game_area_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addLocationsListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.GameLocationArea GameLocationArea')
          ->andWhereIn('GameLocationArea.dm_location_id', $values);
  }

  public function getModelName()
  {
    return 'GameArea';
  }

  public function getFields()
  {
    return array(
      'id'              => 'Number',
      'is_active'       => 'Boolean',
      'name'            => 'Text',
      'code'            => 'Text',
      'image'           => 'ForeignKey',
      'score_to_unlock' => 'Number',
      'locations_list'  => 'ManyKey',
    );
  }
}

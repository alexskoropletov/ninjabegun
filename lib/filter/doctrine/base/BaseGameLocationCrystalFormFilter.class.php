<?php

/**
 * GameLocationCrystal filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameLocationCrystalFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'location' => new sfWidgetFormDoctrineChoice(array('model' => 'GameLocation', 'add_empty' => true)),
      'crystal'  => new sfWidgetFormDoctrineChoice(array('model' => 'GameCrystal', 'add_empty' => true)),
      'chance'   => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'location' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Location'), 'column' => 'id')),
      'crystal'  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Crystal'), 'column' => 'id')),
      'chance'   => new sfValidatorPass(array('required' => false)),
    ));
    

    $this->widgetSchema->setNameFormat('game_location_crystal_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GameLocationCrystal';
  }

  public function getFields()
  {
    return array(
      'id'       => 'Number',
      'location' => 'ForeignKey',
      'crystal'  => 'ForeignKey',
      'chance'   => 'Text',
    );
  }
}

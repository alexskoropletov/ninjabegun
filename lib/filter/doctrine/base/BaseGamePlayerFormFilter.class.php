<?php

/**
 * GamePlayer filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGamePlayerFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active'            => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'                 => new sfWidgetFormDmFilterInput(),
      'gender'               => new sfWidgetFormChoice(array('choices' => array('' => '', 'male' => 'male', 'female' => 'female'))),
      'vk_id'                => new sfWidgetFormDmFilterInput(),
      'image'                => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'image_vk'             => new sfWidgetFormDmFilterInput(),
      'current_location'     => new sfWidgetFormDmFilterInput(),
      'score'                => new sfWidgetFormDmFilterInput(),
      'music_volume'         => new sfWidgetFormDmFilterInput(),
      'sound_volume'         => new sfWidgetFormDmFilterInput(),
      'lives'                => new sfWidgetFormDmFilterInput(),
      'armor'                => new sfWidgetFormDmFilterInput(),
      'energy'               => new sfWidgetFormDmFilterInput(),
      'max_lives'            => new sfWidgetFormDmFilterInput(),
      'max_armor'            => new sfWidgetFormDmFilterInput(),
      'max_energy'           => new sfWidgetFormDmFilterInput(),
      'energy_update'        => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
      'energy_remove'        => new sfWidgetFormChoice(array('choices' => array(
        ''      => '',
        'today' => $this->getI18n()->__('Today'),
        'week'  => $this->getI18n()->__('Past %number% days', array('%number%' => 7)),
        'month' => $this->getI18n()->__('This month'),
        'year'  => $this->getI18n()->__('This year')
      ))),
      'red_crystal'          => new sfWidgetFormDmFilterInput(),
      'green_crystal'        => new sfWidgetFormDmFilterInput(),
      'yellow_crystal'       => new sfWidgetFormDmFilterInput(),
      'white_crystal'        => new sfWidgetFormDmFilterInput(),
      'gray_crystal'         => new sfWidgetFormDmFilterInput(),
      'red_crystal_combo'    => new sfWidgetFormDmFilterInput(),
      'green_crystal_combo'  => new sfWidgetFormDmFilterInput(),
      'yellow_crystal_combo' => new sfWidgetFormDmFilterInput(),
    ));

    $this->setValidators(array(
      'is_active'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'                 => new sfValidatorPass(array('required' => false)),
      'gender'               => new sfValidatorChoice(array('required' => false, 'choices' => array('male' => 'male', 'female' => 'female'))),
      'vk_id'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'                => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Image'), 'column' => 'id')),
      'image_vk'             => new sfValidatorPass(array('required' => false)),
      'current_location'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'score'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'music_volume'         => new sfValidatorPass(array('required' => false)),
      'sound_volume'         => new sfValidatorPass(array('required' => false)),
      'lives'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'armor'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'energy'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'max_lives'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'max_armor'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'max_energy'           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'energy_update'        => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['energy_update']->getOption('choices')))),
      'energy_remove'        => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->widgetSchema['energy_remove']->getOption('choices')))),
      'red_crystal'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'green_crystal'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'yellow_crystal'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'white_crystal'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'gray_crystal'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'red_crystal_combo'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'green_crystal_combo'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'yellow_crystal_combo' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));
    

    $this->widgetSchema->setNameFormat('game_player_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GamePlayer';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'is_active'            => 'Boolean',
      'name'                 => 'Text',
      'gender'               => 'Enum',
      'vk_id'                => 'Number',
      'image'                => 'ForeignKey',
      'image_vk'             => 'Text',
      'current_location'     => 'Number',
      'score'                => 'Number',
      'music_volume'         => 'Text',
      'sound_volume'         => 'Text',
      'lives'                => 'Number',
      'armor'                => 'Number',
      'energy'               => 'Number',
      'max_lives'            => 'Number',
      'max_armor'            => 'Number',
      'max_energy'           => 'Number',
      'energy_update'        => 'Date',
      'energy_remove'        => 'Date',
      'red_crystal'          => 'Number',
      'green_crystal'        => 'Number',
      'yellow_crystal'       => 'Number',
      'white_crystal'        => 'Number',
      'gray_crystal'         => 'Number',
      'red_crystal_combo'    => 'Number',
      'green_crystal_combo'  => 'Number',
      'yellow_crystal_combo' => 'Number',
    );
  }
}

<?php

/**
 * GameResource filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameResourceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active'      => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'           => new sfWidgetFormDmFilterInput(),
      'code'           => new sfWidgetFormDmFilterInput(),
      'type'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'image' => 'image', 'tmx' => 'tmx', 'audio' => 'audio'))),
      'source_file'    => new sfWidgetFormDoctrineChoice(array('model' => 'DmMedia', 'add_empty' => true)),
      'channel'        => new sfWidgetFormChoice(array('choices' => array('' => '', 1 => 1, 2 => 2, 3 => 3, 4 => 4))),
      'locations_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation')),
    ));

    $this->setValidators(array(
      'is_active'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'           => new sfValidatorPass(array('required' => false)),
      'code'           => new sfValidatorPass(array('required' => false)),
      'type'           => new sfValidatorChoice(array('required' => false, 'choices' => array('image' => 'image', 'tmx' => 'tmx', 'audio' => 'audio'))),
      'source_file'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SourceFile'), 'column' => 'id')),
      'channel'        => new sfValidatorChoice(array('required' => false, 'choices' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4))),
      'locations_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'required' => false)),
    ));
    

    $this->widgetSchema->setNameFormat('game_resource_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addLocationsListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.GameLocationResource GameLocationResource')
          ->andWhereIn('GameLocationResource.dm_location_id', $values);
  }

  public function getModelName()
  {
    return 'GameResource';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'is_active'      => 'Boolean',
      'name'           => 'Text',
      'code'           => 'Text',
      'type'           => 'Enum',
      'source_file'    => 'ForeignKey',
      'channel'        => 'Enum',
      'locations_list' => 'ManyKey',
    );
  }
}

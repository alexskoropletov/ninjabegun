<?php

/**
 * GameCrystal filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameCrystalFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active' => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'      => new sfWidgetFormDmFilterInput(),
      'type'      => new sfWidgetFormChoice(array('choices' => array('' => '', 'red' => 'red', 'green' => 'green', 'yellow' => 'yellow', 'white' => 'white', 'gray' => 'gray'))),
    ));

    $this->setValidators(array(
      'is_active' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'      => new sfValidatorPass(array('required' => false)),
      'type'      => new sfValidatorChoice(array('required' => false, 'choices' => array('red' => 'red', 'green' => 'green', 'yellow' => 'yellow', 'white' => 'white', 'gray' => 'gray'))),
    ));
    

    $this->widgetSchema->setNameFormat('game_crystal_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GameCrystal';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'is_active' => 'Boolean',
      'name'      => 'Text',
      'type'      => 'Enum',
    );
  }
}

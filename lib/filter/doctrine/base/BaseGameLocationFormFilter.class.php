<?php

/**
 * GameLocation filter form base class.
 *
 * @package    tgm
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseGameLocationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'is_active'      => new sfWidgetFormChoice(array('choices' => array('' => $this->getI18n()->__('yes or no', array(), 'dm'), 1 => $this->getI18n()->__('yes', array(), 'dm'), 0 => $this->getI18n()->__('no', array(), 'dm')))),
      'name'           => new sfWidgetFormDmFilterInput(),
      'code'           => new sfWidgetFormDmFilterInput(),
      'type'           => new sfWidgetFormChoice(array('choices' => array('' => '', 'home' => 'home', 'world' => 'world', 'arcade' => 'arcade', 'boss' => 'boss'))),
      'resources_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameResource')),
      'areas_list'     => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameArea')),
    ));

    $this->setValidators(array(
      'is_active'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'name'           => new sfValidatorPass(array('required' => false)),
      'code'           => new sfValidatorPass(array('required' => false)),
      'type'           => new sfValidatorChoice(array('required' => false, 'choices' => array('home' => 'home', 'world' => 'world', 'arcade' => 'arcade', 'boss' => 'boss'))),
      'resources_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameResource', 'required' => false)),
      'areas_list'     => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameArea', 'required' => false)),
    ));
    

    $this->widgetSchema->setNameFormat('game_location_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addResourcesListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.GameLocationResource GameLocationResource')
          ->andWhereIn('GameLocationResource.dm_resource_id', $values);
  }

  public function addAreasListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query->leftJoin('r.GameLocationArea GameLocationArea')
          ->andWhereIn('GameLocationArea.dm_area_id', $values);
  }

  public function getModelName()
  {
    return 'GameLocation';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'is_active'      => 'Boolean',
      'name'           => 'Text',
      'code'           => 'Text',
      'type'           => 'Enum',
      'resources_list' => 'ManyKey',
      'areas_list'     => 'ManyKey',
    );
  }
}

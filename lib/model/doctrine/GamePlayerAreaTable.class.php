<?php


class GamePlayerAreaTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GamePlayerArea');
    }
}
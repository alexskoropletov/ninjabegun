<?php


class GameAreaTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameArea');
    }
}
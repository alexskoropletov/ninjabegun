<?php


class GameResourceTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameResource');
    }
}
<?php


class GameLocationResourceTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameLocationResource');
    }
}
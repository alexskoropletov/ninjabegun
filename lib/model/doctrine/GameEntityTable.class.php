<?php


class GameEntityTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameEntity');
    }
}
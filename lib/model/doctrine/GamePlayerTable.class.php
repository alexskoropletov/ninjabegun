<?php


class GamePlayerTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GamePlayer');
    }
}
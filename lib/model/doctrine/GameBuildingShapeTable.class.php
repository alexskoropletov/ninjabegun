<?php


class GameBuildingShapeTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameBuildingShape');
    }
}
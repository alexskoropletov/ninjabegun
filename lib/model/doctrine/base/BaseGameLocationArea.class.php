<?php

/**
 * BaseGameLocationArea
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $dm_location_id
 * @property integer $dm_area_id
 * @property GameLocation $Location
 * @property GameArea $Resource
 * 
 * @method integer          getDmLocationId()   Returns the current record's "dm_location_id" value
 * @method integer          getDmAreaId()       Returns the current record's "dm_area_id" value
 * @method GameLocation     getLocation()       Returns the current record's "Location" value
 * @method GameArea         getResource()       Returns the current record's "Resource" value
 * @method GameLocationArea setDmLocationId()   Sets the current record's "dm_location_id" value
 * @method GameLocationArea setDmAreaId()       Sets the current record's "dm_area_id" value
 * @method GameLocationArea setLocation()       Sets the current record's "Location" value
 * @method GameLocationArea setResource()       Sets the current record's "Resource" value
 * 
 * @package    tgm
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseGameLocationArea extends myDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('game_location_area');
        $this->hasColumn('dm_location_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('dm_area_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));

        $this->option('symfony', array(
             'form' => false,
             'filter' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('GameLocation as Location', array(
             'local' => 'dm_location_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('GameArea as Resource', array(
             'local' => 'dm_area_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));
    }
}
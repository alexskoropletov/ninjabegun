<?php


class GameLocationAreaTable extends myDoctrineTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GameLocationArea');
    }
}
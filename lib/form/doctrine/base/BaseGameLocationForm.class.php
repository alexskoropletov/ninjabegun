<?php

/**
 * GameLocation form base class.
 *
 * @method GameLocation getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameLocationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'is_active'      => new sfWidgetFormInputCheckbox(),
      'name'           => new sfWidgetFormInputText(),
      'code'           => new sfWidgetFormInputText(),
      'type'           => new sfWidgetFormChoice(array('choices' => array('home' => 'home', 'world' => 'world', 'arcade' => 'arcade', 'boss' => 'boss'))),

        'resources_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameResource', 'expanded' => true)),
        'areas_list'     => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameArea', 'expanded' => true)),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active'      => new sfValidatorBoolean(array('required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 255)),
      'code'           => new sfValidatorString(array('max_length' => 255)),
      'type'           => new sfValidatorChoice(array('choices' => array(0 => 'home', 1 => 'world', 2 => 'arcade', 3 => 'boss'), 'required' => false)),
        'resources_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameResource', 'required' => false)),
        'areas_list'     => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameArea', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('game_location[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'GameLocation';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['resources_list']))
    {
      $this->setDefault('resources_list', $this->object->Resources->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['areas_list']))
    {
      $this->setDefault('areas_list', $this->object->Areas->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveResourcesList($con);
    $this->saveAreasList($con);

    parent::doSave($con);
  }

  public function saveResourcesList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['resources_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Resources->getPrimaryKeys();
    $values = $this->getValue('resources_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Resources', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Resources', array_values($link));
    }
  }

  public function saveAreasList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['areas_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Areas->getPrimaryKeys();
    $values = $this->getValue('areas_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Areas', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Areas', array_values($link));
    }
  }

}
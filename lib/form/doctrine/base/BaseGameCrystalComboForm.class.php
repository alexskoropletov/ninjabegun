<?php

/**
 * GameCrystalCombo form base class.
 *
 * @method GameCrystalCombo getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameCrystalComboForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'is_active' => new sfWidgetFormInputCheckbox(),
      'name'      => new sfWidgetFormInputText(),
      'type'      => new sfWidgetFormChoice(array('choices' => array('red' => 'red', 'green' => 'green', 'yellow' => 'yellow'))),
      'bonus'     => new sfWidgetFormChoice(array('choices' => array('armor' => 'armor', 'hp' => 'hp', 'energy' => 'energy'))),

    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active' => new sfValidatorBoolean(array('required' => false)),
      'name'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'type'      => new sfValidatorChoice(array('choices' => array(0 => 'red', 1 => 'green', 2 => 'yellow'), 'required' => false)),
      'bonus'     => new sfValidatorChoice(array('choices' => array(0 => 'armor', 1 => 'hp', 2 => 'energy'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('game_crystal_combo[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'GameCrystalCombo';
  }

}
<?php

/**
 * GamePlayer form base class.
 *
 * @method GamePlayer getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGamePlayerForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'is_active'            => new sfWidgetFormInputCheckbox(),
      'name'                 => new sfWidgetFormInputText(),
      'gender'               => new sfWidgetFormChoice(array('choices' => array('male' => 'male', 'female' => 'female'))),
      'vk_id'                => new sfWidgetFormInputText(),
      'image'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'image_vk'             => new sfWidgetFormInputText(),
      'current_location'     => new sfWidgetFormInputText(),
      'score'                => new sfWidgetFormInputText(),
      'music_volume'         => new sfWidgetFormInputText(),
      'sound_volume'         => new sfWidgetFormInputText(),
      'lives'                => new sfWidgetFormInputText(),
      'armor'                => new sfWidgetFormInputText(),
      'energy'               => new sfWidgetFormInputText(),
      'max_lives'            => new sfWidgetFormInputText(),
      'max_armor'            => new sfWidgetFormInputText(),
      'max_energy'           => new sfWidgetFormInputText(),
      'energy_update'        => new sfWidgetFormDateTime(),
      'energy_remove'        => new sfWidgetFormDateTime(),
      'red_crystal'          => new sfWidgetFormInputText(),
      'green_crystal'        => new sfWidgetFormInputText(),
      'yellow_crystal'       => new sfWidgetFormInputText(),
      'white_crystal'        => new sfWidgetFormInputText(),
      'gray_crystal'         => new sfWidgetFormInputText(),
      'red_crystal_combo'    => new sfWidgetFormInputText(),
      'green_crystal_combo'  => new sfWidgetFormInputText(),
      'yellow_crystal_combo' => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active'            => new sfValidatorBoolean(array('required' => false)),
      'name'                 => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'gender'               => new sfValidatorChoice(array('choices' => array(0 => 'male', 1 => 'female'), 'required' => false)),
      'vk_id'                => new sfValidatorInteger(array('required' => false)),
      'image'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'image_vk'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'current_location'     => new sfValidatorInteger(array('required' => false)),
      'score'                => new sfValidatorInteger(array('required' => false)),
      'music_volume'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'sound_volume'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'lives'                => new sfValidatorInteger(array('required' => false)),
      'armor'                => new sfValidatorInteger(array('required' => false)),
      'energy'               => new sfValidatorInteger(array('required' => false)),
      'max_lives'            => new sfValidatorInteger(array('required' => false)),
      'max_armor'            => new sfValidatorInteger(array('required' => false)),
      'max_energy'           => new sfValidatorInteger(array('required' => false)),
      'energy_update'        => new sfValidatorDateTime(array('required' => false)),
      'energy_remove'        => new sfValidatorDateTime(array('required' => false)),
      'red_crystal'          => new sfValidatorInteger(array('required' => false)),
      'green_crystal'        => new sfValidatorInteger(array('required' => false)),
      'yellow_crystal'       => new sfValidatorInteger(array('required' => false)),
      'white_crystal'        => new sfValidatorInteger(array('required' => false)),
      'gray_crystal'         => new sfValidatorInteger(array('required' => false)),
      'red_crystal_combo'    => new sfValidatorInteger(array('required' => false)),
      'green_crystal_combo'  => new sfValidatorInteger(array('required' => false)),
      'yellow_crystal_combo' => new sfValidatorInteger(array('required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    $this->widgetSchema->setNameFormat('game_player[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'GamePlayer';
  }

}
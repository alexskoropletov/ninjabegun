<?php

/**
 * GameResource form base class.
 *
 * @method GameResource getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameResourceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'is_active'      => new sfWidgetFormInputCheckbox(),
      'name'           => new sfWidgetFormInputText(),
      'code'           => new sfWidgetFormInputText(),
      'type'           => new sfWidgetFormChoice(array('choices' => array('image' => 'image', 'tmx' => 'tmx', 'audio' => 'audio'))),
      'source_file'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SourceFile'), 'add_empty' => true)),
      'channel'        => new sfWidgetFormChoice(array('choices' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4))),

        'locations_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'expanded' => true)),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active'      => new sfValidatorBoolean(array('required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'code'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'type'           => new sfValidatorChoice(array('choices' => array(0 => 'image', 1 => 'tmx', 2 => 'audio'), 'required' => false)),
      'source_file'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SourceFile'), 'required' => false)),
      'channel'        => new sfValidatorChoice(array('choices' => array(0 => 1, 1 => 2, 2 => 3, 3 => 4), 'required' => false)),
        'locations_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'required' => false)),
    ));

    /*
     * Embed Media form for source_file
     */
    $this->embedForm('source_file_form', $this->createMediaFormForSourceFile());
    unset($this['source_file']);

    $this->widgetSchema->setNameFormat('game_resource[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for source_file
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForSourceFile()
  {
    return DmMediaForRecordForm::factory($this->object, 'source_file', 'SourceFile', $this->validatorSchema['source_file']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'source_file');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'source_file');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'source_file', 'SourceFile');
  }

  public function getModelName()
  {
    return 'GameResource';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['locations_list']))
    {
      $this->setDefault('locations_list', $this->object->Locations->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveLocationsList($con);

    parent::doSave($con);
  }

  public function saveLocationsList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['locations_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Locations->getPrimaryKeys();
    $values = $this->getValue('locations_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Locations', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Locations', array_values($link));
    }
  }

}
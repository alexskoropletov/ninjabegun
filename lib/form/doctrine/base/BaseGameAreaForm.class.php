<?php

/**
 * GameArea form base class.
 *
 * @method GameArea getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameAreaForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'is_active'       => new sfWidgetFormInputCheckbox(),
      'name'            => new sfWidgetFormInputText(),
      'code'            => new sfWidgetFormInputText(),
      'image'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'add_empty' => true)),
      'score_to_unlock' => new sfWidgetFormInputText(),

        'locations_list'  => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'expanded' => true)),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active'       => new sfValidatorBoolean(array('required' => false)),
      'name'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'code'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'image'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Image'), 'required' => false)),
      'score_to_unlock' => new sfValidatorInteger(array('required' => false)),
        'locations_list'  => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'GameLocation', 'required' => false)),
    ));

    /*
     * Embed Media form for image
     */
    $this->embedForm('image_form', $this->createMediaFormForImage());
    unset($this['image']);

    $this->widgetSchema->setNameFormat('game_area[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }

  /**
   * Creates a DmMediaForm instance for image
   *
   * @return DmMediaForm a form instance for the related media
   */
  protected function createMediaFormForImage()
  {
    return DmMediaForRecordForm::factory($this->object, 'image', 'Image', $this->validatorSchema['image']->getOption('required'));
  }

  protected function doBind(array $values)
  {
    $values = $this->filterValuesByEmbeddedMediaForm($values, 'image');
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    $values = $this->processValuesForEmbeddedMediaForm($values, 'image');
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
    $this->doUpdateObjectForEmbeddedMediaForm($values, 'image', 'Image');
  }

  public function getModelName()
  {
    return 'GameArea';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['locations_list']))
    {
      $this->setDefault('locations_list', $this->object->Locations->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveLocationsList($con);

    parent::doSave($con);
  }

  public function saveLocationsList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['locations_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Locations->getPrimaryKeys();
    $values = $this->getValue('locations_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Locations', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Locations', array_values($link));
    }
  }

}
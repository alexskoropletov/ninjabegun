<?php

/**
 * GameCrystal form base class.
 *
 * @method GameCrystal getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameCrystalForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'is_active' => new sfWidgetFormInputCheckbox(),
      'name'      => new sfWidgetFormInputText(),
      'type'      => new sfWidgetFormChoice(array('choices' => array('red' => 'red', 'green' => 'green', 'yellow' => 'yellow', 'white' => 'white', 'gray' => 'gray'))),

    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active' => new sfValidatorBoolean(array('required' => false)),
      'name'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'type'      => new sfValidatorChoice(array('choices' => array(0 => 'red', 1 => 'green', 2 => 'yellow', 3 => 'white', 4 => 'gray'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('game_crystal[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'GameCrystal';
  }

}
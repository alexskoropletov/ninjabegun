<?php

/**
 * GameBuildingFigure form base class.
 *
 * @method GameBuildingFigure getObject() Returns the current form's model object
 *
 * @package    tgm
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 */
abstract class BaseGameBuildingFigureForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'is_active'      => new sfWidgetFormInputCheckbox(),
      'name'           => new sfWidgetFormInputText(),
      'shape'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Shape'), 'add_empty' => true)),
      'crystal_combo'  => new sfWidgetFormInputText(),
      'white_crystal'  => new sfWidgetFormInputText(),
      'building_level' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Building'), 'add_empty' => true)),
      'bonus_hp'       => new sfWidgetFormInputText(),
      'bonus_energy'   => new sfWidgetFormInputText(),
      'bonus_armor'    => new sfWidgetFormInputText(),

    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'is_active'      => new sfValidatorBoolean(array('required' => false)),
      'name'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'shape'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Shape'), 'required' => false)),
      'crystal_combo'  => new sfValidatorInteger(array('required' => false)),
      'white_crystal'  => new sfValidatorInteger(array('required' => false)),
      'building_level' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Building'), 'required' => false)),
      'bonus_hp'       => new sfValidatorInteger(array('required' => false)),
      'bonus_energy'   => new sfValidatorInteger(array('required' => false)),
      'bonus_armor'    => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('game_building_figure[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
    
    // Unset automatic fields like 'created_at', 'updated_at', 'position'
    // override this method in your form to keep them
    parent::unsetAutoFields();
  }


  protected function doBind(array $values)
  {
    parent::doBind($values);
  }
  
  public function processValues($values)
  {
    $values = parent::processValues($values);
    return $values;
  }
  
  protected function doUpdateObject($values)
  {
    parent::doUpdateObject($values);
  }

  public function getModelName()
  {
    return 'GameBuildingFigure';
  }

}